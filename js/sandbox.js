
$(document).ready(function() {

	console.log("Entra");

	//$(".elementHide").hide();

	//funcion seleccionar todos los elementos div que poseen la clase "module".
	$("#viewModule").on("click",function() {
		$("div.module").css("color", "red");
		//.html("<p>Div class module selected</p>");
	});

	//funcion seleccionar el elemento label del elemento input utilizando un selector de atributo.
	$("#inputName").on("click",function() {
		$("label[for=fname]").append("<p>Label seleccionado</p>");
	});

	//funcion averiguar cuantos elementos en la página están ocultos (ayuda: .length)
	$("#ocultar").on("click",function() {
		$(".elementHide").hide();
	});

	$("#elementHide").on("click", function(){
		var ocultoleng = $("p.elementHide").length;
		$("p.elementShow").append("<p>Elementos ocultos: " + ocultoleng + "<p/>")
							.css("color", "red");
	});

	//funcion averiguar cuantas imágenes en la página poseen el atributo alt.
	$("#imgAttAlt").on("click", function(){
		var attleng = $("img[alt]").length;
		$(".image").append("<p>Imagen con atributo alt: " + attleng + "</p>")
					.css("color", "red");
	});
	
	//funcion seleccionar todas las filas impares del cuerpo de la tabla.
	$("#imparTable").on("click", function(){
		$('tr:odd').html("<td>impar selected</td>").css("color", "red"); 
	});

	//Seleccionar todas las imágenes en la página; registrar en la consola el atributo alt de cada imagen.
	$("#consoleAttr").on("click", function(){
		$(".image img[alt]").each(function(idx, el) {
	    	console.log('El elemento ' + idx + ' contiene el siguiente atributo: ' + $(el).attr("alt"));
		});
	});

	//Seleccionar el elemento input, luego dirigirse hacia el formulario y añadirle una clase al mismo.
	$("#inputClass").on("click", function(){
		$(".classInput input").addClass("currentInput");
	});

	//Seleccionar el ítem que posee la clase "current" dentro de la lista #myList y remover dicha clase en el elemento; luego añadir la clase "current" al siguiente ítem de la lista.
	$("#liClass").on("click", function(){
		var $li = $("#myList li.current");
		$li.removeClass("current").css("color", "black");

		var $classli= $li.next("li");
		$classli.addClass("current").css("color", "red");
	});

	//Seleccionar el elemento select dentro de #specials; luego dirigirse hacia el botón submit.
	$("#select").on("click", function(){
		$("#specials select").css("color", "black");
		$("#specials :submit").css("color", "red");
	});

	//Seleccionar el primer ítem de la lista en el elemento #slideshow; añadirle la clase "current" al mismo y luego añadir la clase "disabled" a los elementos hermanos.
	$("#slideshowli").on("click", function() {
		var $lifirst= $('#slideshow li:first');
		$lifirst.addClass("current").css("color", "blue"); 

		$lifirst.siblings().addClass("disabled").css("color", "red"); 
	});

	//Añadir 5 nuevos ítems al final de la lista desordenada #myList.
	$("#addli").on("click", function() {
	  	$("#list").append("<li>Agregado 1</li>");
	  	$("#list").append("<li>Agregado 2</li>");
	  	$("#list").append("<li>Agregado 3</li>");
	  	$("#list").append("<li>Agregado 4</li>");
	  	$("#list").append("<li>Agregado 5</li>");
  	});
	
	//Remover los ítems impares de la lista.
	$("#removeli").on("click", function() {
	  	var $target=$("#list li:odd");
	  	$target.remove();
  	});

	//Añadir otro elemento h2 y otro párrafo al último div.module.
	$("#addModule").on("click",function() {
		$("div.addModule:last").append("<h2>Module h2</h2>").css("color", "red");
		$("div.addModule:last").append("<p>Div con clas module</p>>").css("color", "red");
	});

	//Añadir otra opción al elemento select; darle a la opción añadida el valor "Wednesday".
	$("#addday").on("click", function() {
  		$("#week select").append("<option>Wednesday</option>");
  	});

	//Añadir un nuevo div.module a la página después del último; luego añadir una copia de una de las imágenes existentes dentro del nuevo div.
	$("#addDiv").on("click",function() {
		var $divlast= $("<div></div>").appendTo("div:last");

		$(".image img:first").clone().appendTo($divlast);
	});
});